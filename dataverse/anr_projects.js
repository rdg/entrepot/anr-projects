/*
Copyright 2023 Recherche Data Gouv

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/* Tested under Dataverse versions 5.12.1 + 5.14 + 6.0 */

$(document).ready(function() {
    configuregrantNumber();
});

function configuregrantNumber() {

    console.debug("configuregrantNumber called");

    // Add all functions of configuregrantNumber() scope
    //----

    function GetSortOrder(prop) {
        return function(a, b) {
            if (a[prop] > b[prop]) {
                return 1;
            } else if (a[prop] < b[prop]) {
                return -1;
            }
            return 0;
        }
    };

    // Put the text in a result that matches the term in a span with class
    // select2-rendered__match that can be styled (e.g. bold)
    function markMatch(text, term) {

        // Find where the match is
        let match = text.toUpperCase().indexOf(term.toUpperCase());
        let $result = $('<span></span>');
        // If there is no match, move on
        if (match < 0) {
            return $result.text(text);
        }

        // Put in whatever text is before the match
        $result.text(text.substring(0, match));

        // Mark the match
        let $match = $('<span class="select2-rendered__match"></span>');
        $match.text(text.substring(match, match + term.length));

        // Append the matching text
        $result.append($match);

        // Put in whatever is after the match
        $result.append(text.substring(match + term.length));

        return $result;
    }

    // Declare all constants
    //----

    const patternAnrCode = /^ANR-[0-9]{2}-[A-Z]{4}-[0-9]{4}(-[0-9]{2})?$/;

    const agencySelect = 'AgencySelect1';
    const grantNumberSelect = 'grantNumberSelect1';
    const projectIdentifierSelect = 'projectIdentifierSelect1';

    const grantNumberSelectorAgency1 = $($('#metadata_grantNumber').parent().parent().find('.dataset-field-values input.ui-inputfield')[0]);
    const grantNumberSelectorIdentifier1 = $($('#metadata_grantNumber').parent().parent().find('.dataset-field-values input.ui-inputfield')[1]);
    const projectFieldsSelector = $('#metadata_project').parent().parent().find('.dataset-field-values input.ui-inputfield');
    const projectSelectorIdentifier1 = $($(projectFieldsSelector)[0]);

    const freeTextAllowed = true;
    const numberOfResults = 10;
    const placeholderVal = 'Ex. ANR-XX-XXXX-XXXX'; //grantNumberSelectorIdentifier1.attr('placeholder'); to get it from Bundle.properties
    const anrName = 'Agence nationale de la recherche';
    const emptyOption = '<option></option>'; // This empty option is really important for select2 to work well with a created tag and select event triggered
    const showRealInputs = false; // set it to true for debugging purposes

    function isAnrCodeSelected() {
        return patternAnrCode.test(grantNumberSelectorIdentifier1.val());
    }

    // [R3] Clear existing DOM modifications in case of javascript reload (this happens when you repeat a block for instance)
    //----
    if($('#'+grantNumberSelect).length) {
        $('#'+grantNumberSelect).select2('destroy');
        $('#'+agencySelect).select2('destroy');
        $('#'+grantNumberSelect).remove();
        $('#'+agencySelect).remove();
    }
    if($('#'+projectIdentifierSelect).length) {
        $('#'+projectIdentifierSelect).select2('destroy');
        $('#'+projectIdentifierSelect).remove();
    }

    // Add all select that will be turned into select2
    //----

    grantNumberSelectorIdentifier1.parent().append(
    '<select id=' + grantNumberSelect + ' class="form-control add-resource select2" tabindex="-1" aria-hidden="true">'+emptyOption+'</select>');
    grantNumberSelectorIdentifier1.toggle(showRealInputs);

    projectSelectorIdentifier1.parent().append(
    '<select id=' + projectIdentifierSelect + ' class="form-control add-resource select2" tabindex="-1" aria-hidden="true">'+emptyOption+'</select>');
    projectSelectorIdentifier1.toggle(showRealInputs);

    grantNumberSelectorAgency1.parent().append(
    '<select id=' + agencySelect + ' class="form-control add-resource select2" aria-hidden="true">'+emptyOption+'<option value="'+anrName+'">'+anrName+'</option></select>');
    grantNumberSelectorAgency1.toggle(showRealInputs);

    // Init all select2
    //----

    const synchronizedSelect2 = [grantNumberSelect, projectIdentifierSelect];
    synchronizedSelect2.forEach(function (element) {

        let otherSelect = element === grantNumberSelect ? projectIdentifierSelect : grantNumberSelect;

        $('#' + element).select2({
            theme: "classic",
            tags: freeTextAllowed, // "tags" enables free text typing
            createTag: function (params) {
                var term = $.trim(params.term);
                if (term === '') {
                    return null;
                }
                return {
                    id: term,
                    text: term,
                    newTag: true
                }
            },
            insertTag: function (data, tag) {
                if(data.length == 1 && data[0].id === tag.id) {
                    return; // If there is only one matching result, do not insert tag
                }
                if( $('#' + agencySelect).val() === anrName) {
                    return; // [R1.2.1] If ANR Agency is selected free text is not allowed
                }
                // Insert the tag at the start of the results
                data.unshift(tag);
            },
            templateResult: function(item) {
                // No need to template the searching text
                if (item.loading) {
                    return item.id;
                }
                let term = '';
                if (typeof(query) !== 'undefined') {
                    term = query.term;
                }
                // markMatch bolds the search term if/where it
                // appears in the result
                let $result = markMatch(item.id, term);
                return $result;
            },
            templateSelection: function(item) {
                if (item.id === '') { // adjust for custom placeholder values
                    return placeholderVal;
                }
                return item.id;
            },
            language: {
                searching: function(params) {
                    return 'Search by ANR code…';
                }
            },
            placeholder: placeholderVal,
            minimumInputLength: 3,
            allowClear: true,
            // https://select2.org/data-sources/ajax
            ajax: {
                delay: 350,
                transport: function (params, success, failure) { // [R2.1] ajax call is overrided to merge 2 http call results together
                    $.when(
                        // https://www.data.gouv.fr/fr/datasets/anr-01-projets-anr-dos-detail-des-projets-et-des-partenaires/
                        $.ajax({
                            url: "https://tabular-api.data.gouv.fr/api/resources/87d29a24-392e-4a29-a009-83eddcff3e66/data/",
                            data: {
                                page_size: numberOfResults,
                                page: 1,
                                "Projet.Code_Decision_ANR__sort": "asc",
                                "Projet.Code_Decision_ANR__contains": params.data.term
                            },
                            dataType: "json"
                        }),
                        // https://www.data.gouv.fr/fr/datasets/anr-02-projets-anr-dgpie-detail-des-projets-et-des-partenaires/
                        $.ajax({
                            url: "https://tabular-api.data.gouv.fr/api/resources/aca6972b-577c-496a-aa26-009f81256dcb/data/",
                            data: {
                                page_size: numberOfResults,
                                page: 1,
                                "Projet.Code_Decision_ANR__sort": "asc",
                                "Projet.Code_Decision_ANR__contains": params.data.term
                            },
                            dataType: "json"
                        })
                    ).done(function(anr_dos, anr_dgpie) {

                        console.debug(anr_dos, anr_dgpie);

                        let result = anr_dos[0].data.map(function(elem) {
                            return {
                                acronyme: elem["Projet.Acronyme"],
                                id: elem["Projet.Code_Decision_ANR"],
                                titre_fr: elem["Projet.Titre.Francais"],
                                titre_en: elem["Projet.Titre.Anglais"],
                            }
                        });
                        result = result.concat(anr_dgpie[0].data.map(function(elem) {
                              return {
                                 acronyme: elem["Projet.Acronyme"],
                                 id: elem["Projet.Code_Decision_ANR"],
                                 titre_fr: elem["Projet.Titre.Francais"],
                                 titre_en: "",
                              }
                        }));

                        // Results are merged, sorted by anr code and halved
                        result.sort(GetSortOrder("id"));
                        result = result.slice(0, numberOfResults);
                        success(result);

                    }).fail(function (e) {
                        console.error("Error while trying to get ANR Projects", e);
                        success([{"id": "ANR Research Service is temporarily unavailable", "disabled": true}]); // [R2.3]
                    });
                },

                processResults: function(data, page) {
                    console.debug("anr projects combined results", data);
                    return {
                        results: data
                    };
                }
            }
        });

        $('#' + element).on('select2:select', function(e) { // Triggered whenever a result is selected.

            let data = e.params.data;

            if(!data.newTag) { // [R2.4] newTag attribute is defined while using free text therefore project fields must not be updated

                // [R2.2] TODO: manage title language depending on metadata lang selected (see https://guides.dataverse.org/en/latest/installation/config.html#metadatalanguages)
                let title = data.titre_en ? data.titre_en : data.titre_fr;

                // Update grantNumber field value
                grantNumberSelectorIdentifier1.val(data.id);
                projectSelectorIdentifier1.val(data.id);

                // [R2.5.2] Update project fields values
                $(projectFieldsSelector[1]).val(data.acronyme);
                $(projectFieldsSelector[2]).val(title);

                // Update the other select
                $('#' + otherSelect).html($('#' + element).html());
                $('#' + otherSelect).val($('#' + element).val()).trigger('change');

                // [R2.5.1] When ANR code is selected agency select must be updated to ANR entry
                $('#' + agencySelect).val(anrName).trigger("change");
                grantNumberSelectorAgency1.val(anrName);

            } else {
                if(element === grantNumberSelect) {
                    grantNumberSelectorIdentifier1.val(data.id);
                } else if(element === projectIdentifierSelect) {
                    projectSelectorIdentifier1.val(data.id);
                }
            }
        });

        $('#' + element).on('select2:clear', function(e) { //Triggered whenever all selections are cleared.
            $('#' + element).html(emptyOption);
            if(grantNumberSelectorIdentifier1.val() === projectSelectorIdentifier1.val()) { // [R2.4]

                if(isAnrCodeSelected()) { // [R2.6.1]
                    $(projectFieldsSelector[1]).val('');
                    $(projectFieldsSelector[2]).val('');
                }

                grantNumberSelectorIdentifier1.val('');
                projectSelectorIdentifier1.val('');
                $('#' + otherSelect).html(emptyOption);
                $('#' + otherSelect).val('').trigger('change');

            } else if(element === grantNumberSelect) { // [R2.6.2]
                grantNumberSelectorIdentifier1.val('');
            } else if(element === projectIdentifierSelect) { // [R2.6.2]
                projectSelectorIdentifier1.val('');
            }
        });
    });

    $('#' + agencySelect).select2({
        theme: "classic",
        tags: freeTextAllowed, // [R1.1] "tags" enables free text typing
        placeholder: {
            id: '', // the value of the option
            text: grantNumberSelectorAgency1.attr('placeholder')
        },
        allowClear: true
    });

    $('#' + agencySelect).on('select2:select', function(e) { // Triggered whenever a result is selected.
        let data = e.params.data;
        grantNumberSelectorAgency1.val(data.id);

        if(data.id === anrName) {
            if(!isAnrCodeSelected()) { // [R1.2.2]
                $('#' + grantNumberSelect).select2('open');
                $('.select2-search__field').select();
                $('.select2-search__field').val('ANR-');
            }
        } else {
            $('#' + grantNumberSelect).select2('open');
            $('.select2-search__field').select();
            $('.select2-search__field').val('');
        }
    });

    $('#' + agencySelect).on('select2:clear', function(e) { //Triggered whenever all selections are cleared.
        grantNumberSelectorAgency1.val('');
    });

    // Init existing value
    //----
    let existingVal = grantNumberSelectorAgency1.val();
    if(existingVal) {
        if(existingVal === anrName) {
            $('#' + agencySelect).val(anrName).trigger("change");
        } else {
            let newOption = new Option(existingVal, existingVal, true, true);
            $('#' + agencySelect).append(newOption).trigger('change');
        }
    }
    existingVal = grantNumberSelectorIdentifier1.val();
    if(existingVal) {
        let newOption = new Option(existingVal, existingVal, true, true);
        $('#' + grantNumberSelect).append(newOption).trigger('change');
    }
    existingVal = projectSelectorIdentifier1.val();
    if(existingVal) {
        let newOption = new Option(existingVal, existingVal, true, true);
        $('#' + projectIdentifierSelect).append(newOption).trigger('change');
    }

    console.debug("configuregrantNumber loaded");
}