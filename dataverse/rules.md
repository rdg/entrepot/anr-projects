# Rules : 
* **R1** select on agency :
   * **R1.1** select2 on agency help to choose between "Agence nationale de la recherche" or free typing
   * **R1.2** when selecting "Agence nationale de la recherche" :
      * **R1.2.1** select2 on grantNumber and project id won't allow free text therefore will be limited to ANR code search
      * **R1.2.2** select2 on grantNumber will be opened with prefilled *ANR-* value inviting user to search of ANR code
* **R2** select2 on grantNumber and project id will help to search for an ANR code from tabular-api.data.gouv.fr service.
   * **R2.1** select2 are customized to handle double ajax call in order to merge results from 2 files (ANR DOS and ANR DGPIE)
   * **R2.2** english title of ANR Project is prioritised over french title
   * **R2.3** if any of the 2 ajax calls fails, select2 will display a unavailable service error message.
   * **R2.4** select2 on grantNumber and project id will be semi-synchronized : both equals when one ANR code has been selected in either of them.
   * **R2.5** selecting an ANR Code on grantNumber and project id will :
      * **R2.5.1** turn agency field to "Agence nationale de la recherche"
      * **R2.5.2** update project fields
   * **R2.6** clearing grantNumber or project id fields will :
      * **R2.6.1** empty project fields when ANR Code was selected
      * **R2.6.2** otherwise will empty only the cleared field
* **R3** as funding and project are both repeatable field blocks, the code must handle it but will be activated only for the first block of each.